<?php

namespace Drupal\Tests\none_title\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Class NoneTitleTest.
 *
 * Tests for None Title module.
 *
 * @group NoneTitleTest
 */
class NoneTitleTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  public $profile = 'standard';

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'olivero';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'field',
    'node',
    'none_title',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $user = $this->drupalCreateUser([
      'access administration pages',
      'create article content',
      'edit any article content',
      'access content overview',
    ]);
    $this->drupalLogin($user);

    $this->drupalGet('node/add/article');
    $edit = [
      'title[0][value]' => 'My node title',
    ];
    $this->drupalGet('node/add/article');
    $this->submitForm($edit, t('Save'));
  }

  /**
   * Test display of <none> title in nodes.
   */
  public function testNoneTitleNodeDisplay() {
    $this->drupalGet('node/1');
    $this->assertSession()
      ->elementTextContains('css', "h1.page-title span", 'My node title');
    $edit = [
      'title[0][value]' => '<none>',
    ];
    $this->drupalGet('node/1/edit');
    $this->submitForm($edit, t('Save'));
    $this->assertSession()
      ->elementNotExists('css', "h1.page-title span");
    // Check that title <none> along with spaces is also hiding.
    $edit = [
      'title[0][value]' => ' <none> ',
    ];
    $this->drupalGet('node/1/edit');
    $this->submitForm($edit, t('Save'));
    $this->assertSession()
      ->elementNotExists('css', "h1.page-title span");
  }

  /**
   * Test display of <none> title in views.
   */
  public function testNoneTitleViewDisplay() {
    $this->drupalGet('admin/content');
    $this->assertSession()
      ->elementTextContains('css', "table > tbody > tr:nth-child(1) td.views-field-title", 'My node title');
    $edit = [
      'title[0][value]' => '<none>',
    ];
    $this->drupalGet('node/1/edit');
    $this->submitForm($edit, t('Save'));
    $this->drupalGet('admin/content');
    $this->assertSession()
      ->elementNotExists('css', "table > tbody > tr:nth-child(1) td.views-field-title a");
    // Check that title <none> along with spaces is also hiding.
    $edit = [
      'title[0][value]' => ' <none> ',
    ];
    $this->drupalGet('node/1/edit');
    $this->submitForm($edit, t('Save'));
    $this->drupalGet('admin/content');
    $this->assertSession()
      ->elementNotExists('css', "table > tbody > tr:nth-child(1) td.views-field-title a");
  }

}
