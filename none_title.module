<?php

/**
 * @file
 * Primarily using Drupal hook to hide node titles of any node.
 *
 * This is the main module file for None Title.
 */

use Drupal\node\NodeInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_preprocess_field().
 */
function none_title_preprocess_field(&$vars) {
  if ($vars['element']['#field_name'] == 'title' &&
      $vars['element']['#object'] instanceof NodeInterface) {
    foreach ($vars['items'] as &$item) {
      $item['content']['#context']['value'] = (trim($item['content']['#context']['value']) == '<none>')
        ? '' :
        $item['content']['#context']['value'];
    }
  }
}

/**
 * Implements hook_views_plugins_field_alter().
 */
function none_title_views_plugins_field_alter(array &$plugins) {
  $plugins['field']['class'] = 'Drupal\none_title\Plugin\views\field\EntityField';
  $plugins['field']['provider'] = 'none_title';
}

/**
 * Implements hook_help().
 */
function none_title_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.none_title':
      $text = file_get_contents(dirname(__FILE__) . '/README.md');
      if (!\Drupal::moduleHandler()->moduleExists('markdown')) {
        return '<pre>' . $text . '</pre>';
      }
      else {
        // Use the Markdown filter to render the README.
        $filter_manager = \Drupal::service('plugin.manager.filter');
        $settings = \Drupal::configFactory()->get('markdown.settings')->getRawData();
        $config = ['settings' => $settings];
        $filter = $filter_manager->createInstance('markdown', $config);
        return $filter->process($text, 'en');
      }
  }
  return NULL;
}
