None Title
=============

INTRODUCTION
------------
Makes it possible to write <none> in the node title field to hide the node title.

REQUIREMENTS
------------
- Drupal 8
- Node module

INSTALLATION
------------
Install module as usually.

CONFIGURATION
-------------
No configuration is needed.
